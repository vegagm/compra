#!/usr/bin/env pythoh3


habituales = ("patatas","leche","pan")
especificos = []



def main():

    while True:
        elementos = input("Elemento a comprar: ")
        if elementos == '':
            break

        es_repetido = False
        for i in especificos:
            if i == elementos:
                es_repetido = True
        if es_repetido == False:
            especificos.append(elementos)



    print("Lista de la compra: ")
    for i in habituales:
        print(i)
    especificos_no_habituales=0
    for i in especificos:
        es_habitual =False
        for producto_habitual in habituales:
            if i == producto_habitual:
                es_habitual = True
        if es_habitual == False:
            print(i)
            especificos_no_habituales += 1

    print("Elementos habituales: ", len(habituales))
    print("Elementos especificos: ", len(especificos))
    print("Elementos totales: ", len(habituales)+especificos_no_habituales)







if __name__ == '__main__':
    main()